import string
LOWER_START = 1
UPPER_START = 27
ALPHABET_LOWER = list(string.ascii_lowercase)
ALPHABET_UPPER = list(string.ascii_uppercase)

TEST_LIST = ["vJrwpWtwJgWrhcsFMMfFFhFp",
"jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
"PmmdzqPrVvPwwTWBwg",
"wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
"ttgJtRGJQctTZtZT",
"CrZsJsPPZsGzwwsLwLmpwMDw"]

def create_dict(alphabet: list, start: int) -> dict:
        dictionary: dict = {}
        for i,value in enumerate(alphabet):
            dictionary[value] = i + start
        return dictionary

def read_file(path) -> list:
    with open(path, "r") as f:
        lines: list = f.read().splitlines()
    return lines

class Rucksack():

    def __init__(self, items):
        self.items = items
        self.lower_dict: dict = create_dict(ALPHABET_LOWER, LOWER_START)
        self.upper_dict: dict = create_dict(ALPHABET_UPPER, UPPER_START)
        
    

    def search_item_first(self) -> list:
        shared_items: list = []
        for item in self.items:
            first_half: list = list(item[:len(item)//2])
            second_half: list = list(item[len(item)//2:])
            shared_item = [element for element in first_half if element in second_half]
            shared_items.append(shared_item[0])
        return shared_items
    
    def create_list_of_threes(self) -> list:
        list_of_threes: list = []
        whole_list = []
        for index,item in enumerate(self.items):
            item = list(item)
            list_of_threes.append(item)
            if (index + 1) % 3 == 0:
                whole_list.append(list_of_threes)
                list_of_threes = []
        return whole_list

    def search_item_second(self) -> list:
        list_of_threes: list = self.create_list_of_threes()
        shared_items = []
        print(list_of_threes)
        for item in list_of_threes:
            shared_item = [element for element in item[0] if element in item[1] 
                           and element in item[2]][0]
            shared_items.append(shared_item)
        return shared_items
            
                
    
    def give_priority(self, character: str) -> int:
        if character in self.lower_dict.values():
            return self.lower_dict.get(character)
        else:
            return self.upper_dict.get(character)

    def priority_sum(self, shared_items) -> int:
        priorities_sum: int = 0
        for item in shared_items:
            if item == item.lower():
                priorities_sum += self.lower_dict.get(item)
            else:
                priorities_sum += self.upper_dict.get(item)
        return priorities_sum
        
        
    
items = read_file("input_3.txt")

rucksack_1 = Rucksack(items = items)
shared_items = rucksack_1.search_item_second()
print(shared_items)
print(rucksack_1.priority_sum(shared_items))




