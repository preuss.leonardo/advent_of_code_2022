import re

def read_file(path):
    with open(path, "r") as f:
        lines = f.read().splitlines()
    return lines

def create_groups(file):
    elf_snacks = []
    single_elf = []
    for line in file:
        if line == "":
            elf_snacks.append(single_elf)
            single_elf = []
        else:
            single_elf.append(int(line)) #converts to integer
    elf_snacks.append(single_elf) #append the last list
    return elf_snacks

def find_max(group_list):
    sum_of_calories = [sum(group) for group in group_list]
    return max(sum_of_calories)      

#second question
def find_top_n(group_list, n):
    sum_of_calories = [sum(group) for group in group_list]
    sum_of_calories = sorted(sum_of_calories) #sorts list ascending
    return sum(sum_of_calories[-3:]) #returns sum of last 3 items
    
    
    


file = read_file("input_1.txt")
elf_snacks = create_groups(file)
print(find_max(elf_snacks))
print(find_top_n(elf_snacks, 3))