
WIN_SCORE = 6
TIE_SCORE = 3

def read_file(path):
    with open(path, "r") as f:
        lines = f.read().splitlines()
        lines.remove("")
    return lines


class RPS():
    
    def __init__(self, games):
        self.games = games
        self.scores = {"A": 1, "B" : 2, "C" : 3, "X" : 1, "Y" : 2, "Z" : 3}
        self.ties = {"A" : "X", "B" : "Y", "C" : "Z"}
        self.wins = {"A" : "Y", "B": "Z", "C": "X" }
        self.losses = {"A": "Z", "B": "X", "C" : "Y"}
        self.game_scenarios = {"X": 0, "Y": TIE_SCORE, "Z": WIN_SCORE}
        
    
    @staticmethod
    def _create_tuple(str_both_players):
        x, y = str_both_players.split(" ")
        return (x, y)

    def play_game_1(self):
        added_scores = 0
        for game in self.games:
            pair = self._create_tuple(game)
            my_choice = pair[1] #choice of the player
            if pair in self.wins.items():
                added_scores += self.scores.get(my_choice) + WIN_SCORE
            elif pair in self.ties.items():
                added_scores += self.scores.get(my_choice) + TIE_SCORE
            else:
                added_scores += self.scores.get(my_choice)
        
        return added_scores

    def play_game_2(self):
        added_scores = 0
        for game in self.games:
            opponent, outcome = self._create_tuple(game)
            if outcome == "X":
                my_choice = self.losses.get(opponent)
                added_scores += self.scores.get(my_choice) + self.game_scenarios.get(outcome)
            elif outcome == "Y":
                added_scores  += self.scores.get(opponent) + self.game_scenarios.get(outcome)
            else:
                my_choice = self.wins.get(opponent)
                added_scores += self.scores.get(my_choice) + self.game_scenarios.get(outcome)
        
        return added_scores
        
        
    

game_strings = read_file("input_2.txt")
actual_rps = RPS(game_strings)
print(actual_rps.play_game_1())

test_strings = ["A Y", "B X", "C Z"]
test_rps = RPS(test_strings)
print(actual_rps.play_game_2())
