EXAMPLE_PATH = "example_4.txt"
PATH = "input_4.txt"

def read_file(path: str) -> list:
    with open(path, "r") as f:
        lines: list = f.read().splitlines()
    return lines

def get_range(range_str: str) -> list:
    start: str; end: str
    start, end = range_str.split("-")
    start, end = int(start), int(end)
    return [i for i in range(start, end + 1)]
    
def create_list(pairs: list) -> dict:
    pair_list: list = []
    pair: str
    for pair in pairs:
        first: str; last: str
        first, last = pair.split(",")
        first, last = get_range(first), get_range(last)
        pair_list.append((first, last))

    return pair_list

def check_overlaps(pair_list: list) -> int:
    pair: tuple
    count: int = 0
    for pair in pair_list:
        first: list; second: list
        first, second = pair
        #check if one of the lists is a subset of another
        if set(first).issubset(second) or set(second).issubset(first):
            count += 1
    return count
        

def check_overlaps_2(pair_list: list) -> int:
    pair: tuple
    count: int = 0
    for pair in pair_list:
        first: list; second: list
        first, second = pair
        if not set(first).isdisjoint(set(second)):
            count += 1
    
    return count
        
    
lines: list = read_file(PATH)
pair_list =  create_list(lines)
overlap_count = check_overlaps(pair_list)
print(check_overlaps_2(pair_list))