from dataclasses import dataclass
import re

TEST = "example_5.txt"
ACTUAL = "input_5.txt"

def read_file(path: str) -> tuple[list, list, str]:
    with open(path, "r") as lines:
        move_strings: list = []
        crate_strings: list = []
        line: str
        for line in lines:
            if line.startswith("move"):
                move_strings.append(line)
            elif re.search(r"\[[A-Z]\]", line) is not None:
                crate_strings.append(line)
            elif line.startswith(" 1"):
                crate_line: str = line
    return move_strings, crate_strings, crate_line

def parse_cratelist(crate_strings: list, crate_line: str) -> list:
    locations = [i for i, char in enumerate(crate_line) if re.match(r"[0-9]", char) is not None]
    stack_list = [[] for _ in locations]
    for line in crate_strings:
        for index, char in enumerate(line):
            if index in locations and re.match(r"[A-Z]", char) is not None:
                stack_list[locations.index(index)].append(char)
    stack_list = [stack[::-1] for stack in stack_list]
    return stack_list
        
@dataclass
class Move:
    amount: int
    old_pos: int
    new_pos: int

    @staticmethod
    def parse_move(move_string) -> "Move":
        amount, old_pos, new_pos = map(int, re.findall(r"[0-9]+", move_string)) #returns numbers typecasted as int
        return Move(amount, old_pos - 1, new_pos - 1) #take into account zero based indexing

    def cut_list_1(self, current_stack: list, new_stack: list):
        return current_stack[:-self.amount], new_stack + current_stack[-self.amount:][::-1]
    
    def cut_list_2(self, current_stack: list, new_stack: list):
        return current_stack[:-self.amount], new_stack + current_stack[-self.amount:]

    def perform_move(self, crate_list: list, mode: int = 1) -> list:
        current_stack = crate_list[self.old_pos]
        new_stack = crate_list[self.new_pos]
        if mode == 1:
            current_stack, new_stack = self.cut_list_1(current_stack, new_stack)
        else:
            current_stack, new_stack = self.cut_list_2(current_stack, new_stack)
        crate_list[self.old_pos] = current_stack
        crate_list[self.new_pos] = new_stack
        return crate_list


        

def perform_lifting(path: str, mode: int = 1) -> list:
    move_strings, crate_strings, crate_line = read_file(path)
    stack_list = parse_cratelist(crate_strings, crate_line)
    #print(move_strings)
    for action in move_strings:
        current_move = Move.parse_move(action)
        stack_list = current_move.perform_move(stack_list, mode = mode)
    return stack_list
        
for crate in (perform_lifting(ACTUAL)):
    print(crate[-1], end = "")

print("\n")
    
for crate in (perform_lifting(ACTUAL, mode = 2)):
    print(crate[-1], end = "")
    


    
    

