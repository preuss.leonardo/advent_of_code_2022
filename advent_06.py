MARKER_LENGTH = 14
EXAMPLE = "example_06.txt"
ACTUAL = "input_6.txt"

def read_file(path: str) -> str:
    with open(path, "r") as f:
        line = f.read()
    return line

class Signal():
    
    def __init__(self, raw_signal, length):
        self.raw_signal: str = raw_signal
        self.length: int = length
        
    def no_duplicates(self, quadruple) -> bool:
        """check if list has duplicates"""
        return len(quadruple) == len(set(quadruple))
    
    def find_marker(self):
        index: int = MARKER_LENGTH - 1
        while index != len(self.raw_signal):
            curr_quadruple = self.raw_signal[index - self.length + 1 : index + 1]
            if self.no_duplicates(curr_quadruple):
                return index + 1
            
            index += 1
        return None             
                
                
raw_signal = read_file(ACTUAL)
signal_1 = Signal(raw_signal, MARKER_LENGTH)
print(signal_1.find_marker())

        